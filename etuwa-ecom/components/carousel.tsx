import { Carousel } from "react-bootstrap";
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import bannerLogo from '../public/assets/banner1.jpg'

const CarouselComp = () => {
    return (
        <div>
            <Carousel>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src={'https://www.luluhypermarket.in/medias/Web-Groceries.jpg?context=bWFzdGVyfHJvb3R8NDQ2MDc5fGltYWdlL2pwZWd8aGFkL2g4NC85MDIwMTk1NTY5Njk0L1dlYiBHcm9jZXJpZXMuanBnfGFjNWFhZDgxZGIwNTJiYzcyNzczY2Q2MDFkYmE0ZjAxNTE0ZTJmMDQyMjIwOGYyMDI5ZWVkMjA5ZmI3ZTAyNjA'}
                    />
                    <Carousel.Caption>
                    <h3>First slide label</h3>
                    <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src={'https://www.luluhypermarket.in/medias/02.jpg?context=bWFzdGVyfHJvb3R8MTM0MTM1fGltYWdlL2pwZWd8aDg3L2gzMS85MDE2ODg5OTAxMDg2LzAyLmpwZ3wyYzEzOWUwYTYxNjc5NTA1ZTcxNzQ5YWQwMTQyZTBiMGE0NzY2YmFmNTVhNjA2MDg0NTM3NmRkNzE2Y2NmNmQ4'}
                    alt="Second slide"
                    />

                    <Carousel.Caption>
                    <h3>Second slide label</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </Carousel.Caption>
                </Carousel.Item>
            </Carousel>
        </div>
    );
}

export default CarouselComp;