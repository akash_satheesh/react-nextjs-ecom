import styles from '../styles/main.module.css'
import Image from 'next/image'
import Link from 'next/link'
import { homeURL } from '../utilities/urls'
import { parseCartCookie, parseUserCookies } from "../helpers/parseCookie"
import router from 'next/router'
import { useState } from 'react'
import React from 'react'
import { Spinner } from 'react-bootstrap'

const Header = () => {
    const showOrderPreview = () => {
        setLoader(true);
        router.push('/orderpreview');
    }

    const [isLoading, setLoader] = useState(false);
    return (
        <div className={styles.navWrapper}>
            <div className={styles.inlineWrapper}>
                <Link href={homeURL()}><a className={styles.headerLabels}>Fish</a></Link>
                <Link href={homeURL()}><a className={styles.headerLabels}>Meat</a></Link>
                <Link href={homeURL()}><a className={styles.headerLabels}>Fresh</a></Link>
                <Link href={homeURL()}><a className={styles.headerLabels}>Veg</a></Link>
                <Link href={homeURL()}><a className={styles.headerLabels}>Fruits</a></Link>
                <Link href={homeURL()}><a className={styles.headerLabels}>Food</a></Link>
                <Link href={homeURL()}><a className={styles.headerLabels}>Bakery</a></Link>
                <Link href={homeURL()}><a className={styles.headerLabels}>Roastery</a></Link>
                <Link href={homeURL()}><a className={styles.headerLabels}>Non-Food</a></Link>
                <Link href={homeURL()}><a className={styles.headerLabels}>Health and Beauty</a></Link>
                <Link href={homeURL()}><a className={styles.headerLabels}>Chilled</a></Link>
                <Link href={homeURL()}><a className={styles.headerLabels}>House Hold</a></Link>
                <p className={styles.navCaption}> All you need is just one click away... </p>
            </div>
            <div className={`${styles.checkoutWrapper} ${styles.inlineWrapper} `}>
                <span>{parseUserCookies().name}</span>
                <button type="button" className={styles.checkoutButton} onClick={showOrderPreview}>{isLoading ? <Spinner animation="grow" /> : "Checkout"} <span>{parseCartCookie()}</span></button>
            </div>
        </div>
    );
}

export default Header;