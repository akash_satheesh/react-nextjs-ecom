import CopyRight from './copyright'

const Footer = () => {
    return (
        <div>
            <CopyRight />
        </div>
    )

}

export default Footer