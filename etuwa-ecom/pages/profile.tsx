import styles from '../styles/Home.module.css'

const Profile = () => {
    return (
        <div>
            <h2>Profile</h2>
            <div className={styles.profileRow}>
                <div className={styles.profileCol}>
                    <table>
                        <tr>
                            <td>Name</td>
                            <td><input id="name" value="test user" type="text" required /></td>
                        </tr>
                        <tr>
                            <td>Area</td>
                            <td><input id="area" value="Al Waab" type="text" required /></td>
                        </tr>
                        <tr>
                            <td>Address</td>
                            <td><textarea id="present_address" rows={3} cols={20}>test test test</textarea></td>
                        </tr>
                        <tr>
                            <td>Phone No</td>
                            <td><input id="phone_no" value="123456789" type="text" required /></td>
                        </tr>
                    </table>
                </div>
                <div className={styles.profileCol}>
                    <table>
                        <tr>
                            <td>User Name</td>
                            <td><input id="username" value="test@gmail.com" type="text" required /></td>
                        </tr>
                        <tr>
                            <td>Last Login</td>
                            <td><input value="20th Sep '21 12:07 PM" type="text" disabled /></td>
                        </tr>

                    </table>
                </div>
                <div>
                    <button className={styles.btn}>Change Password</button>
                    <button className={styles.btn}>Update</button>
                </div>
            </div>
        </div>
    );
}
export default Profile;