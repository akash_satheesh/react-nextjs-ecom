/**
 Author: Sumith M S
 Component name: OrderPreview
 **/
import React from "react"
import * as cookie from 'cookie'
import { replaceBackSlash } from "../utilities/utilities";
import Image from 'next/image'
import { parseUserCookies } from "../helpers/parseCookie";
import styles from '../styles/orderpreview.module.css';
import itemStyles from '../styles/item.module.css'
import deleteIcon from '../public/assets/deleteicon.jpg'

export async function getServerSideProps(context: any) {
    const parsedCookies = cookie.parse(context.req.headers.cookie);
    let userCookie: string = parsedCookies.user;
    let userCookieObj = JSON.parse(userCookie);
    const res = await fetch('https://qfreshonline.com/androidapp/json/cart?access_token=' + userCookieObj.access_token);
    const data = await res.json();
    return {
        props: { orderItems: data } // will be passed to the page component as props
    }
}


const orderPreview = ({ orderItems }: { orderItems: any }) => {
    return (
        <div>
            <div className={styles.orderPreviewWrapper}>

                <table className={styles.userCard}>
                    <tr>
                        <td><b>Ordered By</b></td>
                        <td><b>Order On</b></td>
                        <td><b>Mobile</b></td>
                        <td><b>Email</b></td>
                    </tr>
                    <tr>
                        <td>{parseUserCookies().name}</td>
                        <td>2021-09-16 13:23:21 </td>
                        <td>{parseUserCookies().phone}</td>
                        <td>{parseUserCookies().email}</td>
                    </tr>
                </table>

                <div className={`${styles.itemDetails} ${styles.itemCard}`}>
                    {orderItems.items.map((orderItem: any) => 
                        <div className={styles.itemListWrapper}  key={orderItem.variant.id}>
                            <table>
                                <tr>
                                    <td className={`${styles.itemFirstData} ${styles.smallTableData} ${styles.itemTableData}`}><Image src={replaceBackSlash(orderItem.variant.main_image)} width={100} height={90} alt="item image" /></td>
                                    <td className={styles.itemTableData}> <h4>{orderItem.variant.name}</h4></td>
                                    <td className={`${styles.smallTableData} ${styles.itemTableData}`}> <h4>{orderItem.variant.price}</h4></td>
                                    <td className={`${styles.addonTableData} ${styles.itemTableData}`}> <h4>{!!orderItem.variant.addon_name ? orderItem.variant.addon_name : "No addon"}</h4></td>
                                    <td className={`${styles.smallTableData} ${styles.itemTableData}`}> <h4>{orderItem.variant.price_formatted}</h4></td>
                                    <td className={`${styles.actionButtonWrapper} ${styles.itemTableData}`}>
                                        <button type="button" className={itemStyles.quantityActionButton} onClick={() => decrementCounter(orderItem)}>-</button>
                                        <input id={`quantity_${orderItem.variant.id}`} type="button" className={itemStyles.quantityActionButton} value="1"></input>
                                        <button type="button" className={itemStyles.quantityActionButton} onClick={() => incrementCounter(orderItem)}>+</button>
                                    </td>
                                    <td className={`${styles.deleteTableRow} ${styles.itemTableData}`}><Image src={deleteIcon} alt="Remove"/></td>
                                </tr>
                            </table>
                        </div>
                    )}
                </div>

            <div className={styles.deliveryCard}>
                <h4>DELIVERY INFORMATION</h4>
                <table>
                    <tr>
                        <td>No.of Items</td>
                        <td>1</td>
                    </tr>
                    <tr>
                        <td>Total</td>
                        <td>168</td>
                    </tr>
                    <hr className={styles.hr} />
                    <tr>
                        <td>Sub Total</td>
                        <td>168</td>
                    </tr>
                    <tr>
                        <td>Delivery Fee</td>
                        <td>Free</td>
                    </tr><hr className={styles.hr} />
                    <tr>
                        <td>Grand Total</td>
                        <td>168</td>
                    </tr>
                    <tr>
                        <td>Area</td>
                        <td>Select</td>
                    </tr><hr className={styles.hr} />
                    <tr>
                        <td>Delivery Time</td>
                        <td>11 : AM</td>
                    </tr>
                    <tr>
                        <td>Payment</td>
                        <td>COD</td>
                    </tr><hr className={styles.hr} />
                    <tr>
                        <td>Instruction</td>
                    </tr>
                    <tr>
                        <td colSpan={4}>tset testtset testtset testtset testtset test</td>
                    </tr>
                </table>

            </div>
           </div> 

        </div>
    );
}

const incrementCounter = (item: any) => {
    let quantityField = document.getElementById(`quantity_${item.id}`) as HTMLInputElement;
    let quantity: number = Number(quantityField.value) + 1;
    quantityField.value = String(quantity);
}

const decrementCounter = (item: any) => {
    let quantityField = document.getElementById(`quantity_${item.id}`) as HTMLInputElement;
    let Initialquantity: number = Number(quantityField.value);
    if (Initialquantity >= 1) {
        let quantity: number = Initialquantity - 1;
        quantityField.value = String(quantity);
    }
}

export default orderPreview;