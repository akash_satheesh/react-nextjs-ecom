import Image from 'next/image'
import { replaceBackSlash } from "../utilities/utilities";
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import styles from '../styles/item.module.css'
import { parseUserCookies } from '../helpers/parseCookie';
import React, { useState } from 'react';
import Link from 'next/link';
import router from 'next/router';
import { Modal, Button } from "react-bootstrap";
import Login from '../components/login';
import { appName } from '../utilities/urls';
import { useCookies } from 'react-cookie';
import Carousel from '../components/carousel'

export async function getServerSideProps(context: any) {
    const id = context.params.id;
    const res = await fetch('https://qfreshonline.com/androidapp/json/Products?category=' + id);
    const data = await res.json();

    if (!data) {
        return {
            notFound: true,
        }
    }

    return {
        props: { itemDetails: data.records } // will be passed to the page component as props
    }
}

const ListItems = ({ itemDetails }: { itemDetails: any }) => {
    let accessToken: string = parseUserCookies().access_token;
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const [cartcookie, setCartCookie, removeCartCookie] = useCookies(["cartcount"]);
        
    const addToCart = async (el: any, accessToken: string) => {
        let quantityField = document.getElementById(`quantity_${el.id}`) as HTMLInputElement;
        let itemQuantity: number = Number(quantityField.value);
        const res = await fetch('https://qfreshonline.com/androidapp/json/addtocart?access_token=' + accessToken, {
            method: 'POST',
            body: JSON.stringify({
                product_id: el.id,
                quantity: itemQuantity,
                addon_id: 0,
            })
        })
        const data = await res.json();
        if (!!data && data.success) {
            let btn = document.getElementById(`addTocart_${el.id}`);
            btn!.innerHTML = 'Added';
            removeCartCookie("cartcount",{
                path: "/"
            });
            setCartCookie("cartcount", JSON.stringify(data.count), {
                path: "/",
                maxAge: 3600, // Expires after 1hr
                sameSite: true,
            });

        } else {
            await router.push('/registration/register');
        }
    };
    
    return (
        <div>
            <Modal show={show} onHide={handleClose} backdrop="static" keyboard={false}>
                <Modal.Header closeButton>
                    <Modal.Title>{appName()}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Login />
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Close
                    </Button>
                    <Button variant="primary" onClick={()=>registerUser()}>Register</Button>
                </Modal.Footer>
            </Modal>
            <div>
                <Carousel />
            </div>
            <div className={styles.itemOuterWrapper}>
                {itemDetails.map((itemDetail: any) =>
                    <div className={styles.itemWrapper} key={itemDetail.id}>
                        <div className={styles.itemInnerWrapper}>
                            <Link href={`\ ${itemDetail.id}`}>
                                <div className={styles.itemImage}>
                                    <Image src={replaceBackSlash(itemDetail.main_image)} width={150} height={150} alt="item image" />
                                </div>
                            </Link>
                            <div className={styles.itemDescWrapper}>
                                <p className={styles.itemName}>{itemDetail.name}</p>
                                <p className={styles.itemArabicName}>{itemDetail.arabic_name}</p>
                                <p className={styles.itemPrice}>{replaceBackSlash(itemDetail.price_formatted)}</p>
                            </div>
                            <div className={styles.itemActionWrapper}>
                                <button type="button" className={styles.quantityActionButton} onClick={() => decrementCounter(itemDetail)}>-</button>
                                <input id={`quantity_${itemDetail.id}`} type="button" className={styles.quantityActionButton} value="1"></input>
                                <button type="button" className={styles.quantityActionButton} onClick={() => incrementCounter(itemDetail)}>+</button>
                                <button id={`addTocart_${itemDetail.id}`} type="button" className={` ${styles.greenButton} ${styles.addToCartButton}`} onClick={(!!accessToken) ? () => addToCart(itemDetail, accessToken) : handleShow}>Add to cart</button>
                            </div>
                        </div>
                    </div>
                )}
            </div>
        </div>
    )

}

const incrementCounter = (item: any) => {
    let quantityField = document.getElementById(`quantity_${item.id}`) as HTMLInputElement;
    let quantity: number = Number(quantityField.value) + 1;
    quantityField.value = String(quantity);
}

const decrementCounter = (item: any) => {
    let quantityField = document.getElementById(`quantity_${item.id}`) as HTMLInputElement;
    let Initialquantity: number = Number(quantityField.value);
    if (Initialquantity >= 1) {
        let quantity: number = Initialquantity - 1;
        quantityField.value = String(quantity);
    }
}

const registerUser = () => {
    router.push('/registration/register');
}


export default ListItems