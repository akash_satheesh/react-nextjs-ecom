/**
 Author: Sumith M S
 Component name: SignupComp
 **/
 import React, { useState } from "react"
 import Login from '../../components/login'
 import Signup from '../../components/signup'
 import styles from '../../styles/registration.module.css'

const SignupComp = () => {
    return (
        <div>
            <div className={styles.registrationBlock}><Signup /></div>
            <div className={` ${styles.registrationBlock} ${styles.loginBlock}`}><Login /></div>
        </div>
    );

}

export default SignupComp;