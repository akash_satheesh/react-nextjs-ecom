import React from 'react';
import { useCookies } from 'react-cookie';

export const parseUserCookies = () => {
  const [cookie, setCookie] = useCookies(["user"]);
  return (!!cookie && !!cookie.user && !!cookie.user.name) ? cookie.user : "";
}

export const parseCartCookie = () => {
  const [cookie, setCookie] = useCookies(["cartcount"]);
  return (!!cookie && !!cookie.cartcount) ? cookie.cartcount : "";
}